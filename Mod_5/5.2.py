# A program that accepts two positive int numbers x and y.  It will return a list of all the even numbers x and y. 

def even_numbers(x, y):
    num = []
    if x > y:
        old = x
        x = y
        y = old
    for ct in range(x, y):
        if ct % 2 == 0:
            num.append(ct)
    return num  

result = even_numbers(6, 16)
print(result)      
        

