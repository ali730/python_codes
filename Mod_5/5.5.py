# A program (function) that asks user to enter an alphabetic string and will than return back an integer number which is the sum of all the UTF-8 codes.

def code_point(s):
    codes = []
    for char in s:
        codes.append(ord(char))
    return sum(codes)

result = code_point("Lets go Python!")
print(result)
   