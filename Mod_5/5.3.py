# Using while loop to ask a user to enter a positive integer and prints a triangle using numbers from 1 to the ct.

my_input = int(input("Please enter an integer: "))

ct = 1

while ct < my_input+1:
    print (str(ct) * ct)
    ct = ct + 1