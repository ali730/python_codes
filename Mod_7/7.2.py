#!/usr/bin/python3

import boto3

# Create EC2 client
ec2 = boto3.client('ec2')

# Describe instances with the specified tag
response = ec2.describe_instances(Filters=[{'Name': 'tag:Name', 'Values': ['YellowTail']}])

# Extract instance IDs
instance_ids = []
for reservation in response['Reservations']:
    for instance in reservation['Instances']:
        instance_ids.append(instance['InstanceId'])

# Terminate instances
if instance_ids:
    ec2.terminate_instances(InstanceIds=instance_ids)
    print("Terminating instances with tag 'Name/YellowTail':", instance_ids)
else:
    print("No instances with tag 'Name/YellowTail' to terminate.")