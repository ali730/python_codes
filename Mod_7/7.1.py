#!/usr/bin/python3

import boto3

# Define the parameters
instance_count = 5
instance_type = 't2.micro'
image_id = 'ami-0f403e3180720dd7e'
tag_key = 'Name'
tag_value = 'YellowTail'

# Create EC2 client
ec2 = boto3.client('ec2')

# Launch EC2 instances
response = ec2.run_instances(
    ImageId=image_id,
    InstanceType=instance_type,
    MinCount=instance_count,
    MaxCount=instance_count,
    TagSpecifications=[
        {
            'ResourceType': 'instance',
            'Tags': [
                {'Key': tag_key, 'Value': tag_value}
            ]
        }
    ]
)

# Extract and print Instance IDs
instance_ids = [instance['InstanceId'] for instance in response['Instances']]
print("Instance IDs created:", instance_ids)