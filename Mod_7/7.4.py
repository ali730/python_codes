#!/usr/bin/python3

import boto3

# Create EC2 client
ec2 = boto3.client('ec2')

# List all running EC2 instances
response_instances = ec2.describe_instances(Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
running_instances = []
for reservation in response_instances['Reservations']:
    for instance in reservation['Instances']:
        running_instances.append(instance['InstanceId'])
print("List of running EC2 instances:", running_instances)

# List all security groups
response_sgs = ec2.describe_security_groups()
all_security_groups = [sg['GroupId'] for sg in response_sgs['SecurityGroups']]
print("List of all security groups:", all_security_groups)

# Identify unused security groups
response_network_interfaces = ec2.describe_network_interfaces()
used_security_groups = set()
for network_interface in response_network_interfaces['NetworkInterfaces']:
    for sg in network_interface['Groups']:
        used_security_groups.add(sg['GroupId'])
unused_security_groups = [sg for sg in all_security_groups if sg not in used_security_groups]
print("List of unused security groups:", unused_security_groups)