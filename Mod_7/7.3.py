#!/usr/bin/python3

import boto3

# Create EC2 client
ec2 = boto3.client('ec2')

# Create security group
response_sg = ec2.create_security_group(
    Description='Allow SSH access',
    GroupName='SSHAccess'
)
security_group_id = response_sg['GroupId']

# Add inbound rule to security group for SSH (port 22)
ec2.authorize_security_group_ingress(
    GroupId=security_group_id,
    IpPermissions=[
        {
            'IpProtocol': 'tcp',
            'FromPort': 22,
            'ToPort': 22,
            'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
        }
    ]
)

# Launch three EC2 instances with the created security group
response_instances = ec2.run_instances(
    ImageId='your_image_id_here',
    InstanceType='t2.micro',
    MinCount=3,
    MaxCount=3,
    SecurityGroupIds=[security_group_id]
)

# Extract instance IDs
instance_ids = [instance['InstanceId'] for instance in response_instances['Instances']]
print("Instance IDs created:", instance_ids)