# This program will give the sum of all even numbers from 1 to 777 using the for loop

total_sum = 0

for number in range(1, 778):
    
    if number % 2 == 0:
        
        total_sum += number
        
print("Sum of even numbers from 1 to 777:", total_sum)

        