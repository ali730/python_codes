#!/usr/bin/python

# The random library gives us functions to randomize selections
import random

input('''############### Yellowtail Python Automation Course - Icebreaker ###############

###                      Press any key to start randomizer.                    ###


#######################################################################################''')

questions_per_student = 3

with open('C:/Users/aliha/OneDrive/Documents/Python Course/Mod_4/roster.txt', 'r', encoding='utf-8') as f:
    students = f.readlines()

# Open icebreaker questions
with open('C:/Users/aliha/OneDrive/Documents/Python Course/Mod_4/icebreaker_questions.txt', 'r', encoding='utf-8') as f:
    questions = f.readlines()

# File output results
results_file = open('Student_Responses.txt', 'w')

while students:
    random_student = random.choice(students)
    print(random_student.strip())
    results_file.write(random_student.strip() + '\'s Questions & Responses\n')

    # Set/reset already_asked question outside of total range of questions bank to handle avoiding repeated question for one student
    already_asked = []

    for _ in range(questions_per_student):
        # Pull a random question
        random_question = random.choice(questions)
        question_index = questions.index(random_question)

        # Before asking question, confirm that it hasn't been asked for student randomize for another question
        if question_index in already_asked:
            print("Selected random question already asked! Next question.... ")
            results_file.write("%d. Student received the same random question!" % (question_index + 1))
        else:
            response = input(random_question)
            results_file.write("\t\t" + response + "\n")
            already_asked.append(question_index)

    # Get index number of randomized student and remove from list
    index = students.index(random_student)
    students.pop(index)

    if students:
        input("press any key to randomize for next student.... ")

results_file.close()
