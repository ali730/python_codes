# This program adds 77 to a number and returns the result.

num = (int(input(f"Please enter a number so this program can add 77 to it: ")))

def user_number(num):
    
    return num + 77
    
result = user_number(num)

print("Adding 77 to", num, "is:", result)

  