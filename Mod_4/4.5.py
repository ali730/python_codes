# This program will allow the user to enter a list of numbers to be add 

numbers_str = (input("Please enter a list of numbers seperated by spaces so I can add them for you: "))

numbers = numbers_str.split()

numbers = [int(numbers_str) for numbers_str in numbers]

def calculate_sum(numbers):
    
    total_sum = 0
    for num in numbers:
        if isinstance(num, int):
            total_sum += num
        else:
            print(f"Ignoring non-integer value: {num}")
    return total_sum

result = calculate_sum(numbers)

print("Sum of numbers in the list you entered:", result)        
        
                