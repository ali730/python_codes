# This program prints the sum of all numbers that are divisible by 7 form the 1 to 777 range excluding 1, 7, and  777.

total = 0 
number = 2

while number <= 776:
    
    if number % 7 == 0 and number !=7 and number !=77:
        
        total += number
    
    number += 1  
    
print("Sum of numbers divisible by 7 (excluding 1, 7, and 777):", total)      