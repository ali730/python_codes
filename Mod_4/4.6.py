def sorted_keys(input_dict):
    # Get the keys of the dictionary
    keys = list(input_dict.keys())
    # Sort the keys
    keys.sort()
    return keys

# Example usage:
my_dict = {'b': 2, 'a': 1, 'c': 3}
sorted_key_list = sorted_keys(my_dict)
print(sorted_key_list)
