# In this program I will create a function that will allow me to add number and it will return the square of the number.

num = (int(input(f"Enter your number here to get the square of the number: ")))
       
def square_number(num):

    return num ** 2


result = square_number(num)

print("Square of", num, "is:", result)