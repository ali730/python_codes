def read_file(filename):
    # Initialize an empty list to store the lines from the file
    lines = []
    
    # Open the file and read its contents line by line
    with open(filename, 'r') as f:
        for line in f:
            # Strip any leading or trailing whitespace and append the line to the list
            lines.append(line.strip())
    
    # Return the list of lines
    return lines

def main():
    # Prompt the user to enter the filename
    filename = input("Enter filename: ")
    
    # Call the function to read the file
    data = read_file(filename)
    
    # Print the list of lines read from the file
    print(data)

if __name__ == "__main__":
    main()
