#!/usr/bin/python

import os

import csv

# Keys are the standardized field n name to be used in consolidated inventory

# Values ate the various field n names found in the different inventory CSV provided by the acquired banks 

standardized_header_mapping = {'Host Name': ['Host Name', 'hostname', 'Name', 'Host'],
                               
                               'IP Address': ['IP Address', 'IP', 'IPAddress'],
                               
                               'Department': ['Department', 'Dept'],
                               
                               'OS': ['OS', 'Operating System'], 
                               
                               'Function': ['Function']} 
consolidated_records = []

# Read inventory files 

parent_dir = "Inventories"

for filename in os.listdir(parent_dir):
    
    print(filename)
    
    with open(parent_dir +'/'+ filename, 'r') as csv_file:
    
      csv_reader = csv.DictReader(csv_file)
    
      for row in csv_reader:
        
        print(row)
        
        modified_record = {}
        
# Write modified and consolidated records into on file  

with open('consolidated_inventory.csv', 'w') as csv_file:
    
    header_names = standardized_header_mapping.keys()
    
    writer = csv.DictReader(csv_file, fieldnames=header_names)
    
    writer.writeheader()  
    
    
    
    for rec in consolidated_records:
        
        writer.writerow(rec)