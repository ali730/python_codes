# This program will will convert any file's content is a list format.

filename = input("Enter file name that you want to convert in list format:  \n")
lines = [ ]
with open(filename) as f:
    lines = [line.rstrip() for line in f]
print(lines)   
