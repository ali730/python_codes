#!/usr/bin/python3

import requests
import os
from bs4 import BeautifulSoup

URL = "https://www.imdb.com/chart/top/?ref_=nv_mv_250"
HEADERS = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36"}

# Print the current working directory
print("Current Working Directory:", os.getcwd())

# Attempt to create the 'Reviews' directory
try:
    os.makedirs("Reviews")
    print("Reviews directory created successfully.")
except Exception as e:
    print("Error creating Reviews directory:", e)

page = requests.get(URL, headers=HEADERS)

if page.status_code != 200:
    print("Unable to find URL")
else:
    print("Page found, extracting movie titles...")
    soup = BeautifulSoup(page.content, "html.parser")
    titles = soup.find_all('td', {'class': "titleColumn"})

    base_url = "https://www.imdb.com"

    for title in titles:
        try:
            # Get title only by pulling out the a tag and create a file name in a directory called Reviews
            f = open("Reviews/" + title.a.text, 'w')
        except FileNotFoundError:
            # Directory not found, create Directory first
            os.makedirs("Reviews")
            print("Reviews directory created successfully.")
            f = open("Reviews/" + title.a.text, 'w')

        # Fetch movie details
        movie_details_url = "{}{}".format(base_url, title.a['href'])
        movie_page = requests.get(movie_details_url, headers=HEADERS)
        movie_soup = BeautifulSoup(movie_page.content, 'html.parser')

        # Find the User reviews href
        ipc_links = movie_soup.find_all('a', {'class': 'ipc-link ipc-link--baseAlt ipc-link-inherit-color'})
        for link in ipc_links:
            if link.text == "User reviews":
                review_link = link['href']
                break

        # Title sub needs to be stripped out to get the FQDN of the Reviews page
        title_sub = "/".join(title.a['href'].split('/', 3)[:3])
        print("Fetching reviews for:", title.a.text)

        # Navigate to the Movie title's review page
        all_reviews_page = requests.get("{}/{}".format(base_url, title_sub, review_link))
        review_soup = BeautifulSoup(all_reviews_page.content, "html.parser")
        reviews = review_soup.find_all('div', {'class': 'text show-more__control'})

        # Write reviews to file
        for review in reviews:
            comment = review.text
            f.write(review.text + "\n")

        print("Reviews saved for:", title.a.text)
        f.close()

print("Script execution complete.")
